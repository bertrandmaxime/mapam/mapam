import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import Button from '../component/Button';


export default function HomeScreen({ navigation }) {
    return (
      <View style={styles.container}>
        <Button style={styles.buttons}
          title="Connexion"
          onPress={() => navigation.navigate('Connexion')}
        />        
        <Button style={styles.buttons}
        title="Inscription"
        onPress={() => navigation.navigate('Enregistrement')}
      />
      <Button style={styles.buttons}
        title="Start New Flight"
        onPress={() => navigation.navigate('Start New Flight')}
      />
      </View>
    );
  };
  const styles = StyleSheet.create({
    container: { 
        flex: 1, 
        alignItems: 'center',
        justifyContent: 'center', 
        display: 'flex', 
        flexDirection: 'column' 
    },
  });
import * as React from 'react';
import { Button, View, Text } from 'react-native';
import OrganiseFlightDetails from '../component/NewFlightInput';  

export default function FlightDetails() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <OrganiseFlightDetails />
      </View>
    );
  }
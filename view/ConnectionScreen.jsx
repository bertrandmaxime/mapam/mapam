import React, { useState } from 'react'
import { Pressable, SafeAreaView, StyleSheet, Switch, Text, TextInput, View } from 'react-native';
import Button from '../component/Button';

export default function LoginForm({ navigation }) {
  const [click, setClick] = useState(false);
  const { username, setUsername } = useState("");
  const { password, setPassword } = useState("");
  return (
    <SafeAreaView style={styles.container}>

      <Text style={styles.title}>Connexion</Text>
      <View style={styles.inputView}>
        <TextInput style={styles.input} placeholder='Email' value={username} onChangeText={setUsername} autoCorrect={false}
          autoCapitalize='none' />
        <TextInput style={styles.input} placeholder='Mot de passe' secureTextEntry value={password} onChangeText={setPassword} autoCorrect={false}
          autoCapitalize='none' />
      </View>
      <View style={styles.rememberView}>
        <View style={styles.switch}>
          <Switch value={click} onValueChange={setClick} trackColor={{ true: '#67C6E3', false: "gray" }} />
          <Text style={styles.rememberText}>Se souvenir de moi</Text>
        </View>
        <View>
          <Pressable onPress={() => navigation.navigate("Connect or Create")}>
            <Text style={styles.forgetText}>Mot de passe oublie ?</Text>
          </Pressable>
        </View>
      </View>

      <View style={styles.buttonView}>
        <Button
          title="Connexion"
          onPress={() => navigation.navigate('Connect or Create')} />
      </View>
    </SafeAreaView>
  )
}


const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    paddingTop: 70,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    textTransform: "uppercase",
    textAlign: "center",
    paddingVertical: 40,
    color: 'black'
  },
  inputView: {
    gap: 15,
    width: "100%",
    paddingHorizontal: 40,
    marginBottom: 5
  },
  input: {
    height: 50,
    paddingHorizontal: 20,
    backgroundColor: "#DFF5FF",
    borderRadius: 50
  },
  rememberView: {
    width: "100%",
    paddingHorizontal: 50,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    marginBottom: 8
  },
  switch: {
    flexDirection: "row",
    gap: 1,
    justifyContent: "center",
    alignItems: "center"

  },
  forgetText: {
    fontSize: 11,
    color: "#67C6E3"
  },
  buttonView: {
    width: "100%",
    paddingHorizontal: 50
  },
})
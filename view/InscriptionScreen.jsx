import React, { useState } from 'react'
import { Pressable, SafeAreaView, StyleSheet, Switch, Text, TextInput, View } from 'react-native';
import Button from '../component/Button';

export default function RegisterForm({ navigation }) {
  const { username, setUsername } = useState("");
  const { password, setPassword } = useState("");
  const { name, setName } = useState("");
  const { surname, setSurname } = useState("");
  const { operator, setOperator } = useState("");
return (
  <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Inscription</Text>
      <View style={styles.inputView}>
          <TextInput style={styles.input} placeholder='Nom' value={name} onChangeText={setName} autoCorrect={false}
      autoCapitalize='none' />
          <TextInput style={styles.input} placeholder='Prénom' value={surname} onChangeText={setSurname} autoCorrect={false}
      autoCapitalize='none' />
          <TextInput style={styles.input} placeholder='Email' value={username} onChangeText={setUsername} autoCorrect={false}
      autoCapitalize='none' />
          <TextInput style={styles.input} placeholder='Mot de passe' secureTextEntry value={password} onChangeText={setPassword} autoCorrect={false}
      autoCapitalize='none'/>
          <TextInput style={styles.input} placeholder='Confirmation du Mot de passe' secureTextEntry value={password} onChangeText={setPassword} autoCorrect={false}
      autoCapitalize='none'/>
          <TextInput style={styles.input} placeholder='Operateur' value={operator} onChangeText={setOperator} autoCorrect={false}
      autoCapitalize='none' />
      
      </View>

      <View style={styles.buttonView}>
          <Button
          title="Inscription"
          onPress={() => navigation.navigate('Connect or Create')}/>
      </View>
  </SafeAreaView>
)
}


const styles = StyleSheet.create({
  container : {
    alignItems : "center",
    paddingTop: 70,
  },
  title : {
    fontSize : 30,
    fontWeight : "bold",
    textTransform : "uppercase",
    textAlign: "center",
    paddingVertical : 40,
    color : 'black'
  },
  inputView : {
    gap : 15,
    width : "100%",
    paddingHorizontal : 40,
    marginBottom  :5
  },
  input : {
    height : 50,
    paddingHorizontal : 20,
    backgroundColor : "#DFF5FF",
    borderRadius: 50
  },
  rememberView : {
    width : "100%",
    paddingHorizontal : 50,
    justifyContent: "space-between",
    alignItems : "center",
    flexDirection : "row",
    marginBottom : 8
  },
  switch :{
    flexDirection : "row",
    gap : 1,
    justifyContent : "center",
    alignItems : "center"
    
  },
  forgetText : {
    fontSize : 11,
    color : "#67C6E3"
  }, 
  buttonView :{
    width :"100%",
    paddingHorizontal : 50
  },
  })
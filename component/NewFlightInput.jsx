import React from 'react';
import { View, StyleSheet } from 'react-native';
import Button from './Button';


export default function OrganiseFlightDetails({ navigation }) {
    return (
      <View style={styles.container}>
        <Button style={styles.buttons}
          title="F-HRAT, P2010"
          // onPress={() => navigation.navigate('DetailslFlight')}
        />
        <text style = {styles.keepCyan}>F-HRAT, P2010</text>
        
        <Button style={styles.buttons}
        title="Carburant"
        // onPress={() => navigation.navigate('DetailsFlight')}
      />
      </View>
    );
  };
  const styles = StyleSheet.create({
    container: { 
        flex: 1, 
        alignItems: 'center',
        justifyContent: 'center', 
        display: 'flex', 
        flexDirection: 'column' 
    },
    keepCyan : {
      borderRadius:10,
      color: 'blue',
      backgroundColor:'DFF5FF',
      fontFamily: 'Inter',
      fontWeight: 'bold',
      fontSize:24,
    }
  });
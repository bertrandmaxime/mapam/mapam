import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './view/HomeScreen';
import FlightDetails from './view/FlightDetails';
import ConnectionScreen from './view/ConnectionScreen';
import RegisterForm from './view/InscriptionScreen';


const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home"   screenOptions={{headerShown: false}}>
        <Stack.Screen name="Connect or Create" component={HomeScreen} />
        <Stack.Screen name="Start New Flight" component={FlightDetails} />
        <Stack.Screen name="Connexion" component={ConnectionScreen} />
        <Stack.Screen name="Enregistrement" component={RegisterForm} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
